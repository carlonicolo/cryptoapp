const electron = require('electron')
const path = require('path')
const BrowserWindow = electron.remote.BrowserWindow
const axios = require('axios')
const shell = require('electron').shell

/**
 * Use APIs to get a specified number of news from the endpoint
 * @param {Int} number_of_news number of news to request.
 */
function getNews(number_of_news){
    axios.get('https://min-api.cryptocompare.com/data/v2/news/?feeds=cryptocompare,cointelegraph,coindesk')
        .then(res => {

            console.log("Before cryptos. This is res: ", res)
            const info = res.data.Data
            console.log("After cryptos. This is cryptos: ", info)

            console.log("Title: ", info[0].title)
            console.log("url: ", info[0].url)
            console.log("imageurl: ", info[0].imageurl)
            console.log("source: ", info[0].source)
            console.log("body: ", info[0].body)

            

            for(var i=0; i < number_of_news; i++){
                console.log("imageurl: ", info[i].imageurl)
                image_url = "url("+info[i].imageurl+")"
                console.log("Formatted imageurl: ", image_url)

                document.getElementById("info-container"+i).style.backgroundImage = image_url;
                //document.getElementById("image").style.backgroundImage = image_url;
                
                console.log("Title: ", info[i].title)
                console.log("guid url: ", info[i].guid)
                tit = info[i].title
                sub_tit = tit.substring(0,50);
                console.log("This is the value of the sub_tit: ", sub_tit)

                document.getElementById('title'+i).innerHTML = sub_tit
                document.getElementById('url'+i).innerHTML = info[i].url
            }
        })
    }

    /**
     * This function is used to open an external link to the address website
     * specified inside the id name passed as element.
     * @param {string} element id name of the caller 
     */
    function externalshell(element){

        //take the last element of the string element in this case the number title0 the 0
        // because in this way i know that the link is in url0
        //str.slice(-1);
        var id = element.id;
        console.log("id value before slice(): ", id)
        var last_element_id = id.slice(-1)
        console.log("Last element id: ", last_element_id)
        var url_id = "url"+last_element_id
        shell.openExternal(document.getElementById(url_id).textContent)
      }

//Get five news      
getNews(20)
//60 minutes interval
setInterval(getNews, 3600000);