const electron = require('electron')
const path = require('path')
const BrowserWindow = electron.remote.BrowserWindow
const axios = require('axios')
const shell = require('electron').shell
//const ipc = electron.ipcRenderer

//const notifyBtn = document.getElementById('notifyBtn')
var price = document.querySelector('h1')
//var targetPrice = document.getElementById('targetPrice')
//var targetPriceVal

//const notification = {
//    title: 'BTC Alert',
//    body: 'BTC just beat your target price!',
//    icon: path.join(__dirname, '../assets/images/btc.png')
//}

function getCryptoInfo() {
    // console.log("Inside function getBTC()")
    // axios.get('https://min-api.cryptocompare.com/data/pricemulti?fsyms=BTC&tsyms=USD')
    //axios.get('https://api.coinmarketcap.com/v2/ticker/?limit=10')
    axios.get('https://api.coinmarketcap.com/v2/ticker/?convert=EUR&limit=10')
        .then(res => {

        console.log("Before cryptos. This is res: ", res)
        const cryptos = res.data.data;
        console.log("After cryptos. This is cryptos: ", cryptos)


        //Object keys
        var object_keys = Object.keys(cryptos);
        console.log("object_keys", object_keys)
        console.log("object_keys type: ",typeof(object_keys))
        // console.log("object_keys[3]", object_keys[3])

        var array_cryptos = []
        for(var a = 0; a < object_keys.length; a++){
            array_cryptos.push(cryptos[object_keys[a]])
            console.log("Object inserted into array_cryptos: ", cryptos[object_keys[a]])
            console.log("Element a:", a, " for array_cryptos[a]: ", cryptos[object_keys[a]])
        }
        console.log("array_cryptos: ",array_cryptos)
        
        array_cryptos.sort(function (a, b) {
            return a.rank - b.rank;
          });
        
        console.log("array_criptos sorted by rank: ", array_cryptos)

        //index of cryptos object
        for (let key in cryptos) {
            console.log(key + ':', cryptos[key]);
        }

        

        // console.log("Rank of object index 4 : ", cryptos[512].name)

        for(var i=0;  i < array_cryptos.length; i++){
            console.log("Object: ", array_cryptos[i])

            console.log(array_cryptos[i].symbol)
            document.getElementById('symbol'+i).innerHTML = "[ "+array_cryptos[i].symbol+" ]"

            console.log("Object: ", array_cryptos[i], " rank element: ", array_cryptos[i].rank)
            document.getElementById('rank'+i).innerHTML = "Rank: "+array_cryptos[i].rank

            console.log(array_cryptos[i].name)
            document.getElementById('crypto'+i).innerHTML = "<img src='https://chasing-coins.com/api/v1/std/logo/"+array_cryptos[i].symbol+"' />" +"<br>"+ array_cryptos[i].name

            console.log(array_cryptos[i].quotes.USD.price)
            document.getElementById('price_usd_'+i).innerHTML = "$ "+array_cryptos[i].quotes.USD.price.toFixed(4)

            console.log(array_cryptos[i].quotes.EUR.price)
            document.getElementById('price_eur_'+i).innerHTML = "€ "+array_cryptos[i].quotes.EUR.price.toFixed(4)
            

            console.log(array_cryptos[i].quotes.USD.percent_change_1h)
            var x = parseFloat(array_cryptos[i].quotes.USD.percent_change_1h);
            console.log("Type(x)", typeof(x));
            console.log("Value of x:", x);
            var el_1h = document.getElementById('percentage_1h_'+i);
            if(x > 0){
                console.log("percentage_1h_", i," Varibale:",  x," > 0")
                el_1h.style.color="#4ce626"
                document.getElementById('percentage_1h_'+i).innerHTML = "1h: "+array_cryptos[i].quotes.USD.percent_change_1h.toFixed(2)
            }else{
                console.log("percentage_1h_", i," Varibale:",  x," < 0")
                el_1h.style.color="#f50909"
                document.getElementById('percentage_1h_'+i).innerHTML = "1h: "+array_cryptos[i].quotes.USD.percent_change_1h.toFixed(2)
            }
            
            
            console.log(array_cryptos[i].quotes.USD.percent_change_24h)
            var y = parseFloat(array_cryptos[i].quotes.USD.percent_change_24h);
            var el_24h = document.getElementById('percentage_24h_'+i)
            if(y > 0){
                el_24h.style.color="#4ce626"
                document.getElementById('percentage_24h_'+i).innerHTML = "24h: "+array_cryptos[i].quotes.USD.percent_change_24h.toFixed(2)
            }else{
                el_24h.style.color="#f50909"
                document.getElementById('percentage_24h_'+i).innerHTML = "24h: "+array_cryptos[i].quotes.USD.percent_change_24h.toFixed(2)
            }
            
            console.log(array_cryptos[i].quotes.USD.percent_change_7d)
            var z = parseFloat(array_cryptos[i].quotes.USD.percent_change_7d);
            var el_7d = document.getElementById('percentage_7d_'+i)
            if(z > 0){
                el_7d.style.color="#4ce626"
                document.getElementById('percentage_7d_'+i).innerHTML = "7d: "+array_cryptos[i].quotes.USD.percent_change_7d.toFixed(2)
            }else{
                el_7d.style.color="#f50909"
                document.getElementById('percentage_7d_'+i).innerHTML = "7d: "+array_cryptos[i].quotes.USD.percent_change_7d.toFixed(2)
            }


        }
  
        })
}
// getCryptoInfo()
//Interval 5 minutes 
// setInterval(getCryptoInfo, 300000);

//Interval 30 sec 
// setInterval(getCryptoInfo, 30000);


function getNews(){
    axios.get('https://min-api.cryptocompare.com/data/v2/news/?feeds=cryptocompare,cointelegraph,coindesk')
        .then(res => {

            console.log("Before cryptos. This is res: ", res)
            const info = res.data.Data
            console.log("After cryptos. This is cryptos: ", info)

            console.log("Title: ", info[0].title)
            console.log("url: ", info[0].url)
            console.log("imageurl: ", info[0].imageurl)
            console.log("source: ", info[0].source)
            console.log("body: ", info[0].body)

            

            for(var i=0; i < 5; i++){
                console.log("imageurl: ", info[i].imageurl)
                image_url = "url("+info[i].imageurl+")"
                console.log("Formatted imageurl: ", image_url)

                document.getElementById("info-container"+i).style.backgroundImage = image_url;
                //document.getElementById("image").style.backgroundImage = image_url;
                
                console.log("Title: ", info[i].title)
                console.log("guid url: ", info[i].guid)
                tit = info[i].title
                sub_tit = tit.substring(0,50);
                console.log("This is the value of the sub_tit: ", sub_tit)

                document.getElementById('title'+i).innerHTML = sub_tit
                document.getElementById('url'+i).innerHTML = info[i].url
                

                // document.getElementById('url').addEventListener('click', function(event){
                //     var element_value = document.getElementById('url').textContent;
                //     console.log("Value of url: ", element_value)
                //     shell.openExternal(element_value)
                // })

                // console.log("Source: ", info[i].source)
                // document.getElementById("source"+i).innerHTML = info[i].source

                
            }


            
            })
}

getCryptoInfo()
getNews()

//5 min interval
setInterval(getCryptoInfo, 300000);

//60 minutes interval
setInterval(getNews, 3600000);










