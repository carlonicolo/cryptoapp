const electron = require('electron')
const path = require('path')
const BrowserWindow = electron.remote.BrowserWindow
const axios = require('axios')


function createElements(rows){
    console.log("Inside function createElements()")
    var n = rows/5;
    var box = 5;

    for(var j=0; j<n; j++){
        //document.getElementById('content_page').innerHTML = "<div id='row"+j+"' class='row'>"

        var div_row = document.createElement("DIV");
        div_row.className = "row";
        //div_content_page.idName = 'row'+j
        div_row.id ="row"+j
        //var text_row = document.createTextNode("This is a row div.");
        //div_row.appendChild(text_row);
        document.getElementById("content").appendChild(div_row);
    }
    
    //this variable is used to increment the internal iDs
    var increment_id = 0

    
    for(var z=0; z<n; z++){
                
        for(var y=0; y<5;y++){
            var div_price_container = document.createElement("DIV");
            div_price_container.className = "price-container";
            div_price_container.id = "price-container"+z+"-"+y
            //var text_price_container = document.createTextNode("This is a row div.");
            //div_price_container.appendChild(text_price_container);
            document.getElementById("row"+z).appendChild(div_price_container);
            
            var p_crypto = document.createElement("P");
            p_crypto.id = "crypto"+increment_id
            p_crypto.className = "subtext";
            var text_p_crypto = document.createTextNode("Loading...");
            p_crypto.appendChild(text_p_crypto);
            document.getElementById("price-container"+z+"-"+y).appendChild(p_crypto);
            
            var p_symbol = document.createElement("H6");
            p_symbol.id = "symbol"+increment_id
            var text_p_symbol = document.createTextNode("Loading...");
            p_symbol.appendChild(text_p_symbol);
            document.getElementById("price-container"+z+"-"+y).appendChild(p_symbol);

            var p_rank = document.createElement("H6");
            p_rank.id = "rank"+increment_id
            var text_p_rank = document.createTextNode("Loading...");
            p_rank.appendChild(text_p_rank);
            document.getElementById("price-container"+z+"-"+y).appendChild(p_rank);

            var p_price_usd = document.createElement("H6");
            p_price_usd.id = "price_usd_"+increment_id
            var text_p_price_usd = document.createTextNode("Loading...");
            p_price_usd.appendChild(text_p_price_usd);
            document.getElementById("price-container"+z+"-"+y).appendChild(p_price_usd);

            var p_price_eur = document.createElement("H6");
            p_price_eur.id = "price_eur_"+increment_id
            var text_p_price_eur = document.createTextNode("Loading...");
            p_price_eur.appendChild(text_p_price_eur);
            document.getElementById("price-container"+z+"-"+y).appendChild(p_price_eur);

            var p_percentage_1h = document.createElement("H6");
            p_percentage_1h.id = "percentage_1h_"+increment_id
            var text_p_percentage_1h = document.createTextNode("Loading...");
            p_percentage_1h.appendChild(text_p_percentage_1h);
            document.getElementById("price-container"+z+"-"+y).appendChild(p_percentage_1h);
            
            var p_percentage_24h = document.createElement("H6");
            p_percentage_24h.id = "percentage_24h_"+increment_id
            var text_p_percentage_24h = document.createTextNode("Loading...");
            p_percentage_24h.appendChild(text_p_percentage_24h);
            document.getElementById("price-container"+z+"-"+y).appendChild(p_percentage_24h);
            
            var p_percentage_7d = document.createElement("H6");
            p_percentage_7d.id = "percentage_7d_"+increment_id
            var text_p_percentage_7d = document.createTextNode("Loading...");
            p_percentage_7d.appendChild(text_p_percentage_7d);
            document.getElementById("price-container"+z+"-"+y).appendChild(p_percentage_7d);
            
            
            increment_id++
        }
    }
}


function getCryptoInfo(limit) {
    // console.log("Inside function getBTC()")
    // axios.get('https://min-api.cryptocompare.com/data/pricemulti?fsyms=BTC&tsyms=USD')
    //axios.get('https://api.coinmarketcap.com/v2/ticker/?limit=10')
    // https://api.coinmarketcap.com/v2/ticker/?convert=EUR&limit=20&sort=rank
    axios.get('https://api.coinmarketcap.com/v2/ticker/?convert=EUR&limit='+limit+'&sort=rank')
        .then(res => {

        console.log("Before cryptos. This is res: ", res)
        const cryptos = res.data.data;
        console.log("After cryptos. This is cryptos: ", cryptos)

        //Object keys
        var object_keys = Object.keys(cryptos);
        console.log("object_keys", object_keys)
        console.log("object_keys type: ",typeof(object_keys))
        // console.log("object_keys[3]", object_keys[3])
        
        /**
         * create cryptos array
         */
        var array_cryptos = []
        for(var a = 0; a < object_keys.length; a++){
            array_cryptos.push(cryptos[object_keys[a]])
            console.log("Object inserted into array_cryptos: ", cryptos[object_keys[a]])
            console.log("Element a:", a, " for array_cryptos[a]: ", cryptos[object_keys[a]])
        }
        console.log("array_cryptos: ",array_cryptos)
        
        //sort array
        array_cryptos.sort(function (a, b) {
            return a.rank - b.rank;
          });
        
        console.log("array_criptos sorted by rank: ", array_cryptos)

        for (let key in cryptos) {
            console.log(key + ':', cryptos[key]);
        }

        // console.log("Rank of object index 4 : ", cryptos[512].name)

        for(var i=0;  i < array_cryptos.length; i++){
            console.log("Object: ", array_cryptos[i])

            console.log(array_cryptos[i].rank)
            document.getElementById('rank'+i).innerHTML = "Rank: "+array_cryptos[i].rank

            console.log(array_cryptos[i].name)
            document.getElementById('crypto'+i).innerHTML = "<img src='https://chasing-coins.com/api/v1/std/logo/"+array_cryptos[i].symbol+"' />" +"<br>"+ array_cryptos[i].name
            
            console.log(array_cryptos[i].symbol)
            document.getElementById('symbol'+i).innerHTML = "[ "+array_cryptos[i].symbol+" ]"

            console.log(array_cryptos[i].quotes.USD.price)
            document.getElementById('price_usd_'+i).innerHTML = "$ "+array_cryptos[i].quotes.USD.price.toFixed(4)

            console.log(array_cryptos[i].quotes.EUR.price)
            document.getElementById('price_eur_'+i).innerHTML = "€ "+array_cryptos[i].quotes.EUR.price.toFixed(4)
            

            console.log(array_cryptos[i].quotes.USD.percent_change_1h)
            var x = parseFloat(array_cryptos[i].quotes.USD.percent_change_1h);
            console.log("Type(x)", typeof(x));
            console.log("Value of x:", x);
            var el_1h = document.getElementById('percentage_1h_'+i);
            if(x > 0){
                console.log("percentage_1h_", i," Varibale:",  x," > 0")
                el_1h.style.color="#4ce626"
                document.getElementById('percentage_1h_'+i).innerHTML = "1h: "+array_cryptos[i].quotes.USD.percent_change_1h.toFixed(2)
            }else{
                console.log("percentage_1h_", i," Varibale:",  x," < 0")
                el_1h.style.color="#f50909"
                document.getElementById('percentage_1h_'+i).innerHTML = "1h: "+array_cryptos[i].quotes.USD.percent_change_1h.toFixed(2)
            }
            
            
            console.log(array_cryptos[i].quotes.USD.percent_change_24h)
            var y = parseFloat(array_cryptos[i].quotes.USD.percent_change_24h);
            var el_24h = document.getElementById('percentage_24h_'+i)
            if(y > 0){
                el_24h.style.color="#4ce626"
                document.getElementById('percentage_24h_'+i).innerHTML = "24h: "+array_cryptos[i].quotes.USD.percent_change_24h.toFixed(2)
            }else{
                el_24h.style.color="#f50909"
                document.getElementById('percentage_24h_'+i).innerHTML = "24h: "+array_cryptos[i].quotes.USD.percent_change_24h.toFixed(2)
            }
            
            console.log(array_cryptos[i].quotes.USD.percent_change_7d)
            var z = parseFloat(array_cryptos[i].quotes.USD.percent_change_7d);
            var el_7d = document.getElementById('percentage_7d_'+i)
            if(z > 0){
                el_7d.style.color="#4ce626"
                document.getElementById('percentage_7d_'+i).innerHTML = "7d: "+array_cryptos[i].quotes.USD.percent_change_7d.toFixed(2)
            }else{
                el_7d.style.color="#f50909"
                document.getElementById('percentage_7d_'+i).innerHTML = "7d: "+array_cryptos[i].quotes.USD.percent_change_7d.toFixed(2)
            }


        }
  
        })
}


/**
 * Get info of cryptocurrency passed as parameter
 * @param {string} cryptoName cryptocurrency symbol for which get info
 */
function getCryptoInfoByName(cryptoName) {
    // console.log("Inside function getBTC()")
    // axios.get('https://min-api.cryptocompare.com/data/pricemulti?fsyms=BTC&tsyms=USD')
    //axios.get('https://api.coinmarketcap.com/v2/ticker/?limit=10')
    // https://api.coinmarketcap.com/v2/ticker/?convert=EUR&limit=20&sort=rank
    axios.get('https://api.coinmarketcap.com/v2/ticker/?convert=EUR&limit=100&sort=rank')
        .then(res => {

        console.log("Before cryptos. This is res: ", res)
        const cryptos = res.data.data;
        console.log("After cryptos. This is cryptos: ", cryptos)

        //Object keys
        var object_keys = Object.keys(cryptos);
        console.log("object_keys", object_keys)
        console.log("object_keys type: ",typeof(object_keys))
        // console.log("object_keys[3]", object_keys[3])
        

        for (let key in cryptos) {
            console.log(key + ':', cryptos[key]);
        }

        var y = 0;
        var found = "";

        while(y < object_keys.length){
            if(cryptos[object_keys[y]].symbol == cryptoName){
                console.log("Value of cryptos[object_keys[y]].symbol = ", cryptos[object_keys[y]].symbol)
                console.log("cryptos[object_keys[y]].symbol = ", cryptoName)
                
                console.log(cryptos[object_keys[y]].rank)
                document.getElementById('rank').innerHTML = "Rank: "+cryptos[object_keys[y]].rank
    
                console.log(cryptos[object_keys[y]].name)
                document.getElementById('crypto').innerHTML = "<img src='https://chasing-coins.com/api/v1/std/logo/"+cryptos[object_keys[y]].symbol+"' />" + " " + cryptos[object_keys[y]].name
                
                console.log(cryptos[object_keys[y]].symbol)
    
                console.log(cryptos[object_keys[y]].quotes.USD.price)
                document.getElementById('price_usd').innerHTML = "$ "+cryptos[object_keys[y]].quotes.USD.price.toFixed(4)
    
                console.log(cryptos[object_keys[y]].quotes.EUR.price)
                document.getElementById('price_eur').innerHTML = "€ "+cryptos[object_keys[y]].quotes.EUR.price.toFixed(4)
                
    
                console.log(cryptos[object_keys[y]].quotes.USD.percent_change_1h)
                var x = parseFloat(cryptos[object_keys[y]].quotes.USD.percent_change_1h);
                console.log("Type(x)", typeof(x));
                console.log("Value of x:", x);
                var el_1h = document.getElementById('percentage_1h');
                if(x > 0){
                    console.log("percentage_1h", y," Varibale:",  x," > 0")
                    el_1h.style.color="#4ce626"
                    document.getElementById('percentage_1h').innerHTML = "1h: "+cryptos[object_keys[y]].quotes.USD.percent_change_1h.toFixed(2)
                }else{
                    console.log("percentage_1h", y," Varibale:",  x," < 0")
                    el_1h.style.color="#f50909"
                    document.getElementById('percentage_1h').innerHTML = "1h: "+cryptos[object_keys[y]].quotes.USD.percent_change_1h.toFixed(2)
                }
                
                
                console.log(cryptos[object_keys[y]].quotes.USD.percent_change_24h)
                var p_24 = parseFloat(cryptos[object_keys[y]].quotes.USD.percent_change_24h);
                var el_24h = document.getElementById('percentage_24h')
                if(p_24 > 0){
                    el_24h.style.color="#4ce626"
                    document.getElementById('percentage_24h').innerHTML = "24h: "+cryptos[object_keys[y]].quotes.USD.percent_change_24h.toFixed(2)
                }else{
                    el_24h.style.color="#f50909"
                    document.getElementById('percentage_24h').innerHTML = "24h: "+cryptos[object_keys[y]].quotes.USD.percent_change_24h.toFixed(2)
                }
                
                console.log(cryptos[object_keys[y]].quotes.USD.percent_change_7d)
                var z = parseFloat(cryptos[object_keys[y]].quotes.USD.percent_change_7d);
                var el_7d = document.getElementById('percentage_7d')
                if(z > 0){
                    el_7d.style.color="#4ce626"
                    document.getElementById('percentage_7d').innerHTML = "7d: "+cryptos[object_keys[y]].quotes.USD.percent_change_7d.toFixed(2)
                }else{
                    el_7d.style.color="#f50909"
                    document.getElementById('percentage_7d').innerHTML = "7d: "+cryptos[object_keys[y]].quotes.USD.percent_change_7d.toFixed(2)
                }
                
                found = "Symbol found";
                return found
                //y = object_keys.length;
            }
            console.log("cryptos[object_keys[y]].symbol != cryptoName",cryptos[object_keys[y]].symbol," =! ", cryptoName)
            console.log("Inside while loop.")
            y++;
        }
        console.log("Value of found: ", found);

       
  
        })
}


function searchCrypto(){
    var element_val = document.getElementById("myText").value;
    var upper_element_val = element_val.toUpperCase();
    console.log("Value of element_val: ", upper_element_val);
    getCryptoInfoByName(upper_element_val);
}





/**
 * This function selects the value choosen in select, then erase the
 * content and call the function to create the structure and the
 * function to insert the content.
 */
function displayCrypto(){
    var x = document.getElementById("mySelect").value;
    var x_number = parseInt(x);
    console.log("Value of selection: ", x);
    console.log("Type of x: ", typeof(x));
    console.log("Value of x_number: ", x_number);
    console.log("Type of x_number: ", typeof(x_number));
    
    //Delete content children
    var myNode = document.getElementById("content");
    myNode.innerHTML = '';
    
    createElements(x_number);
    getCryptoInfo(x_number);
}

createElements(20)
getCryptoInfo(20)
